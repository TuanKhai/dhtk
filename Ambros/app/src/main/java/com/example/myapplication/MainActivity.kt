package com.example.myapplication

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream


class MainActivity : AppCompatActivity() {

    lateinit var readBtn: Button
    lateinit var writeBtn: Button
    lateinit var fileContentEdt: EditText
    private val fileName = "a.txt"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        readBtn = findViewById(R.id.readBtn)
        writeBtn = findViewById(R.id.writeBtn)
        fileContentEdt = findViewById(R.id.fileContentEdt)
        readBtn.setOnClickListener {
            readStringFromFile(fileName)
        }
        writeBtn.setOnClickListener {
            writeStringToFile(fileName, fileContentEdt.text.toString())
        }
    }

    private fun readStringFromFile(fileName: String) {
        val fileInputStream : FileInputStream
        try {
            fileInputStream = openFileInput(fileName)
            fileInputStream.bufferedReader().use {
                val res = it.readText()
                fileContentEdt.setText(res)
            }
        } catch (e: Exception){
            e.printStackTrace()
        }
    }
    private fun writeStringToFile(fileName: String, content: String) {
        val fileOutputStream: FileOutputStream
        try {
            fileOutputStream = openFileOutput(fileName, Context.MODE_PRIVATE)
            fileOutputStream.write(content.toByteArray())
            Toast.makeText(applicationContext,"File $fileName write success!",Toast.LENGTH_LONG).show()
        } catch (e: Exception){
            e.printStackTrace()
        }
    }

}